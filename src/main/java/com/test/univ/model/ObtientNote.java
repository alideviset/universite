package com.test.univ.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ObtientNote {
    @Id

    private String 	CodeA ;
    private String Semestre ;
    private String NomDomaine;
    private String CIN ;
}
