package com.test.univ.controller;


import com.test.univ.model.Mention;

import com.test.univ.repository.MentionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class MentionController {
    @Autowired
    MentionRepository repository;

    public List<Mention> getAllMentions() {
        System.out.println("Get all Mention...");
        List<Mention> Mention = new ArrayList<>();
        repository.findAll().forEach(Mention::add);
        return Mention;
    }
}
