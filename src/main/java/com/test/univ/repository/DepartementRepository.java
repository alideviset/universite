package com.test.univ.repository;

import com.test.univ.model.Departement;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepartementRepository extends JpaRepository<Departement,String> {
    Page<Departement> findByDepartementCodedept(String codeetab, Pageable pageable);
  /*  Page<Departement> findAll(Pageable pageable);

    Page<Departement> findByLibfrContaining(String libfr, Pageable pageable);*/
}
