package com.test.univ.controller;

import com.test.univ.model.Bac;
import com.test.univ.model.Etudiant;
import com.test.univ.model.Gouvernorat;
import com.test.univ.repository.EtudiantRepository;
import com.test.univ.repository.GouvernoratRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/gouvernorats")
public class GouvernoratController {
    @Autowired
    private GouvernoratRepository repository;

    @PostMapping("/savegouvernorat")
    public ResponseEntity<Gouvernorat> createGouvernorat(@RequestBody Gouvernorat gouvernorat) {
        try {
            Gouvernorat _gouvernorat = repository
                    .save(new Gouvernorat(gouvernorat.getCodegouv(), gouvernorat.getLibgouv()));
            return new ResponseEntity<>(_gouvernorat, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping("/listgouvernorat")
    public ResponseEntity<List<Gouvernorat>> getAllGouvernorats(@RequestParam(required = false) String libgouv) {
        try {
            List<Gouvernorat> gouvernorats = new ArrayList<Gouvernorat>();

            if (libgouv == null)
                repository.findAll().forEach(gouvernorats::add);
            else
                repository.findByLibgouv(libgouv).forEach(gouvernorats::add);

            if (gouvernorats.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(gouvernorats, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping("/gouvernorat/{codegouv}")
    public ResponseEntity<Gouvernorat> getTutorialById(@PathVariable("codegouv") String codegouv) {
        Optional<Gouvernorat> gouvernorat = repository.findById(codegouv);

        if (gouvernorat.isPresent()) {
            return new ResponseEntity<>(gouvernorat.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/updategouvernorat/{codegouv}")
    public ResponseEntity<Gouvernorat> updateBac(@PathVariable("codegouv") String codegouv, @RequestBody Gouvernorat gouvernorat) {
        Optional<Gouvernorat> gouvernorat1 = repository.findById(codegouv);

        if (gouvernorat1.isPresent()) {
            Gouvernorat _gouvernorat = gouvernorat1.get();
            _gouvernorat.setCodegouv(gouvernorat.getCodegouv());
            _gouvernorat.setLibgouv(gouvernorat.getLibgouv());

            return new ResponseEntity<>(repository.save(_gouvernorat), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/deletegouvernorat/{codegouv}")
    public ResponseEntity<String> deleteGouvernorat(@PathVariable("codegouv") String codegouv){
        repository.deleteById(codegouv);
        return new ResponseEntity<String>("Gouvernorat with '"+codegouv+"' has been deleted",HttpStatus.OK);
    }

    @DeleteMapping("/deleteallgouvernorat")
    public ResponseEntity<HttpStatus> deleteAllGouvernorats() {
        try {
            repository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

