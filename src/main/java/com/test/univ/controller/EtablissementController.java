package com.test.univ.controller;

import com.test.univ.model.Etablissement;

import com.test.univ.model.Organisme;
import com.test.univ.repository.EtablissementRepository;

import com.test.univ.repository.OrganismeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


import java.net.URI;
import java.util.*;

@RestController
@RequestMapping("/api/v1/etablissements")
public class EtablissementController {
    @Autowired
    EtablissementRepository repository;
    @Autowired
    OrganismeRepository organismeRepository;
    @Autowired
    public EtablissementController(EtablissementRepository repository, OrganismeRepository organismeRepository) {
        this.repository = repository;
        this.organismeRepository = organismeRepository;
    }
    @PostMapping

        public ResponseEntity<Etablissement> create(@RequestBody Etablissement etablissement) {
        Optional<Organisme> org = organismeRepository.findById(etablissement.getOrganisme().getCodeorg());
        if (org.isEmpty()) {
            return ResponseEntity.unprocessableEntity().build();
        }
        etablissement.setOrganisme(org.get());
        Etablissement etab = repository.save(etablissement);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{codeetab}")
                .buildAndExpand(etab.getCodeetab()).toUri();
        return ResponseEntity.created(location).body(etab);
    }
    @PutMapping("/{codeetab}")
    public ResponseEntity<Etablissement> update(@RequestBody @Validated Etablissement etablissement, @PathVariable String codeetab) {
        Optional<Organisme> optionalOrganisme = organismeRepository.findById(etablissement.getOrganisme().getCodeorg());
        if (!optionalOrganisme.isPresent()) {
            return ResponseEntity.unprocessableEntity().build();
        }

        Optional<Etablissement> optionalEtablissement = repository.findById(codeetab);
        if (!optionalEtablissement.isPresent()) {
            return ResponseEntity.unprocessableEntity().build();
        }

        etablissement.setOrganisme(optionalOrganisme.get());
        etablissement.setCodeetab(optionalEtablissement.get().getCodeetab());
        repository.save(etablissement);

        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{codeetab}")
    public ResponseEntity<Etablissement> delete(@PathVariable String codeetab) {
        Optional<Etablissement> optionalEtablissement = repository.findById(codeetab);
        if (!optionalEtablissement.isPresent()) {
            return ResponseEntity.unprocessableEntity().build();
        }
        repository.delete(optionalEtablissement.get());
        return ResponseEntity.noContent().build();
    }

    @GetMapping
    public ResponseEntity<Page<Etablissement>> getAll(Pageable pageable) {
        return ResponseEntity.ok(repository.findAll(pageable));
    }

    @GetMapping("/{codeetab}")
    public ResponseEntity<Etablissement> getById(@PathVariable String codeetab) {
        Optional<Etablissement> optionalBook = repository.findById(codeetab);
        if (!optionalBook.isPresent()) {
            return ResponseEntity.unprocessableEntity().build();
        }
        return ResponseEntity.ok(optionalBook.get());
    }
    @GetMapping("/organisme/{OrgCode}")
    public ResponseEntity<Page<Etablissement>> getByLibraryId(@PathVariable String OrgCode, Pageable pageable) {
        return ResponseEntity.ok(repository.findByOrganismeCodeorg(OrgCode, pageable));
    }

    @GetMapping("/libfrs")
    public ResponseEntity<Map<String, Object>> getAllEtabs(
            @RequestParam(required = false) String libfr,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "3") int size
    ) {

        try {
            List<Etablissement> etablissements = new ArrayList<Etablissement>();
            Pageable paging = PageRequest.of(page, size);

            Page<Etablissement> pageTuts;
            if (libfr == null)
                pageTuts = repository.findAll(paging);
            else
                pageTuts = repository.findByLibfrContaining(libfr, paging);

            etablissements = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("establishments", etablissements);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
