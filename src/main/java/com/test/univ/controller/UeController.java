package com.test.univ.controller;


import com.test.univ.model.UE;

import com.test.univ.repository.UeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UeController {
    @Autowired
    UeRepository repository;

    public List<UE> getAllUes() {
        System.out.println("Get all UE...");
        List<UE> UE = new ArrayList<>();
        repository.findAll().forEach(UE::add);
        return UE;
    }
}
