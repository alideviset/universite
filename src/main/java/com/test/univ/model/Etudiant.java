package com.test.univ.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Etudiant implements Serializable {

    @EmbeddedId
    private EtudiantPK etudiantPK;
    private String nomar ;
    private String prenomar ;
    private String nomfr ;
    private String prenomfr ;
    private String gender;
    private String StatutFam ;
    private Date dateNaiss;
    private String lieunaissar;
    private String lieunaissfr;
   /* @ManyToOne
    private Bac bac;
    @ManyToOne
    private Gouvernorat gouvernorat;*/

    //private String statut;
    //private String gouvcode;
    //private String codebac;

}
