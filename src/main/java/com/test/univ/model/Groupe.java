package com.test.univ.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Groupe {
    @Id

    private String codeannee;
    private String codegrpe;
    private String speccode;
    private String semestre;
}
