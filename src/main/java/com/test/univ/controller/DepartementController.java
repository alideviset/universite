package com.test.univ.controller;



import com.test.univ.model.Departement;
import com.test.univ.model.Etablissement;

import com.test.univ.repository.DepartementRepository;

import com.test.univ.repository.EtablissementRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.*;

@RestController
@RequestMapping("/api/v1/departements")
public class DepartementController {
    @Autowired
    DepartementRepository departementRepository;
    @Autowired
    EtablissementRepository etablissementRepository;

    @Autowired
    public DepartementController(DepartementRepository departementRepository, EtablissementRepository etablissementRepository) {
        this.departementRepository = departementRepository;
        this.etablissementRepository = etablissementRepository;
    }

    @PostMapping

    public ResponseEntity<Departement> create(@RequestBody Departement departement) {
        Optional<Etablissement> etab = etablissementRepository.findById(departement.getEtablissement().getCodeetab());
        if (etab.isEmpty()) {
            return ResponseEntity.unprocessableEntity().build();
        }
        departement.setEtablissement(etab.get());
        Departement dept = departementRepository.save(departement);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{codedept}")
                .buildAndExpand(dept.getCodedept()).toUri();
        return ResponseEntity.created(location).body(dept);
    }
   @PutMapping("/{codedept}")
    public ResponseEntity<Departement> update(@RequestBody @Validated Departement departement, @PathVariable String codedept) {
        Optional<Etablissement> optionalEtablissement = etablissementRepository.findById(departement.getEtablissement().getCodeetab());
        if (!optionalEtablissement.isPresent()) {
            return ResponseEntity.unprocessableEntity().build();
        }

        Optional<Departement> optionalDepartement = departementRepository.findById(codedept);
        if (!optionalDepartement.isPresent()) {
            return ResponseEntity.unprocessableEntity().build();
        }

        departement.setEtablissement(optionalEtablissement.get());
        departement.setCodedept(optionalDepartement.get().getCodedept());
        departementRepository.save(departement);

        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{codedept}")
    public ResponseEntity<Departement> delete(@PathVariable String codedept) {
        Optional<Departement> optionalDepartement = departementRepository.findById(codedept);
        if (!optionalDepartement.isPresent()) {
            return ResponseEntity.unprocessableEntity().build();
        }
        departementRepository.delete(optionalDepartement.get());
        return ResponseEntity.noContent().build();
    }

    @GetMapping
    public ResponseEntity<Page<Departement>> getAll(Pageable pageable) {
        return ResponseEntity.ok(departementRepository.findAll(pageable));
    }

    @GetMapping("/{codedept}")
    public ResponseEntity<Departement> getById(@PathVariable String codedept) {
        Optional<Departement> optionalBook = departementRepository.findById(codedept);
        if (!optionalBook.isPresent()) {
            return ResponseEntity.unprocessableEntity().build();
        }
        return ResponseEntity.ok(optionalBook.get());
    }

    @GetMapping("/etablissement/{codeetab}")
    public ResponseEntity<Page<Departement>> getByEtablissementId(@PathVariable String codeetab, Pageable pageable) {
        return ResponseEntity.ok(departementRepository.findByDepartementCodedept(codeetab, pageable));
    }
/*
    @GetMapping("/libfrs")
    public ResponseEntity<Map<String, Object>> getAllDepts(
            @RequestParam(required = false) String libfr,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "3") int size
    ) {

        try {
            List<Departement> departements = new ArrayList<Departement>();
            Pageable paging = PageRequest.of(page, size);

            Page<Departement> pageTuts;
            if (libfr == null)
                pageTuts = departementRepository.findAll(paging);
            else
                pageTuts = departementRepository.findByLibfrContaining(libfr, paging);

            departements = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("departement", departements);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }*/
}
