package com.test.univ.repository;

import com.test.univ.model.Etablissement;
import com.test.univ.model.Organisme;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrganismeRepository extends JpaRepository<Organisme,String> {
    Page<Organisme> findAll(Pageable pageable);
    Page<Organisme> findByLibfrContaining(String libfr, Pageable pageable);
}
