package com.test.univ.repository;


import com.test.univ.model.UE;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UeRepository extends JpaRepository<UE,String> {
}
