package com.test.univ.repository;

import com.test.univ.model.Inscription;

import com.test.univ.model.InscriptionPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InscriptionRepository extends JpaRepository<Inscription, InscriptionPK> {
}
