package com.test.univ.repository;

import com.test.univ.model.Gouvernorat;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GouvernoratRepository extends JpaRepository<Gouvernorat,String> {
    //List<Gouvernorat> findByPublished(boolean published);
    List<Gouvernorat> findByLibgouv(String libgouv);
}
