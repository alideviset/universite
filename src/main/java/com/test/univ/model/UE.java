package com.test.univ.model;

import lombok.*;
import net.bytebuddy.asm.Advice;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UE {
    @Id
    private String codeue;
    private String codeanne;
    private String speccode;
    private String semestre;
    private String libfr;
    private String Nature;
    private String abrev;
    private Float creditue;
    private Float coefue;
    private String regime;
    @ManyToOne
    @JoinColumn(name = "spec_code")
    private Specialite specialite;
}
