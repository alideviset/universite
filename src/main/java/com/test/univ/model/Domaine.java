package com.test.univ.model;

import lombok.*;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Domaine {
    @Id
    private String codedom;
    private String libfr;
    private String libar;
    private String logo;
    private String codeanne;
    @ManyToOne
    @JoinColumn(name = "dept_code")
    private Departement departement;
    @OneToMany(mappedBy = "domaine",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private Collection<Mention> mentions;
}
