package com.test.univ.controller;

import com.test.univ.model.Etudiant;
import com.test.univ.model.Inscription;
import com.test.univ.repository.EtudiantRepository;
import com.test.univ.repository.InscriptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/etudiant")
public class EtudiantController {
    @Autowired
    private EtudiantRepository repository;

    @PostMapping
    public Etudiant save(@RequestBody Etudiant etudiant){
        return repository.save(etudiant);
    }

    @GetMapping
    public List<Etudiant> getAll(){
        return repository.findAll();
    }
}
