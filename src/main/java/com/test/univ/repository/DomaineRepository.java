package com.test.univ.repository;

import com.test.univ.model.Domaine;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DomaineRepository extends JpaRepository<Domaine,String> {
}
