package com.test.univ.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Mention implements Serializable {
    @Id
    private String codement;
    private String codeanne;
    private String libfr;
    private String libar;
    @ManyToOne
    @JoinColumn(name = "dom_code")
    private Domaine domaine;
    @OneToMany(mappedBy = "mention",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private Collection<Specialite> mentions;
}
