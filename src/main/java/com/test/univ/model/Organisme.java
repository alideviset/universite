package com.test.univ.model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;


@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Organisme implements Serializable {
    @Id
    private String codeorg;
    private String libfr;
    private String libar;
    private String logo;
    @OneToMany(mappedBy = "organisme",fetch =FetchType.LAZY,cascade = CascadeType.ALL)
    //@JsonIgnore
    private Collection<Etablissement> etablissements ;
    @JsonIgnore
    public Collection<Etablissement> getEtablissements() {
        return etablissements;
    }


}
