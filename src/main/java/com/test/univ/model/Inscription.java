package com.test.univ.model;

import lombok.*;

import javax.persistence.*;

import java.io.Serializable;


@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class Inscription implements Serializable {

    @EmbeddedId
    private InscriptionPK inscriptionPK;
    private String  statut ;

}
