package com.test.univ.repository;

import com.test.univ.model.Annee;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AnneeRepository extends JpaRepository<Annee,String> {
}
