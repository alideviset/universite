package com.test.univ.repository;

import com.test.univ.model.EC;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EcRepository extends JpaRepository<EC,String> {
}
