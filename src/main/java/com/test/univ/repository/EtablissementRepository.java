package com.test.univ.repository;

import com.test.univ.model.Etablissement;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.awt.print.Book;
import java.util.Optional;

public interface EtablissementRepository extends JpaRepository<Etablissement,String> {
    Page<Etablissement> findAll(Pageable pageable);
    Page<Etablissement> findByOrganismeCodeorg(String OrgCode, Pageable pageable);
    Page<Etablissement> findByLibfrContaining(String libfr, Pageable pageable);
   /*@Modifying
    @Transactional
    @Query("DELETE FROM Etablissement e WHERE e.Organisme.codeorg = ?1")
    void deleteByCodeorg(String OrgCode);*/
}
