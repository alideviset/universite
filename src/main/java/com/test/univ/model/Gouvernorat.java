package com.test.univ.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Gouvernorat implements Serializable {
    @Id

    private String codegouv;
    private String libgouv ;
    /*@OneToMany(mappedBy = "gouvernorat",cascade = CascadeType.ALL)
    private Collection<Etudiant> etudiants;*/
}
