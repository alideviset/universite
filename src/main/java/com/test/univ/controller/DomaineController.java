package com.test.univ.controller;


import com.test.univ.model.Domaine;
import com.test.univ.repository.DomaineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class DomaineController {
    @Autowired
    DomaineRepository repository;

    public List<Domaine> getAllDomaines() {
        System.out.println("Get all Domaine...");
        List<Domaine> Domaine = new ArrayList<>();
        repository.findAll().forEach(Domaine::add);
        return Domaine;
    }
}
