package com.test.univ.repository;

import com.test.univ.model.AffectationSemestre;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AffectaionSemestreRepository extends JpaRepository<AffectationSemestre,String> {
}
