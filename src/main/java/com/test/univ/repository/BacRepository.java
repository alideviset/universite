package com.test.univ.repository;

import com.test.univ.model.Bac;
import com.test.univ.model.Gouvernorat;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface BacRepository extends JpaRepository<Bac,String> {
   List<Bac> findByLibbac(String libbac);
 /*@Query("select b from Bac b where b.libbac like :x")
   Page<Bac> listlibbac(  String libbac, Pageable p);*/
}
