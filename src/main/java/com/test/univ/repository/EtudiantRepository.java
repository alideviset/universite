package com.test.univ.repository;

import com.test.univ.model.Etudiant;
import com.test.univ.model.EtudiantPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EtudiantRepository extends JpaRepository<Etudiant, EtudiantPK> {
}
