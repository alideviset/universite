package com.test.univ.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;


@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Bac implements Serializable {
    @Id
    private String codebac;
    private String libbac ;
  /*  @OneToMany(mappedBy = "bac",cascade = CascadeType.ALL)
    //@JoinColumn(name = "etd_code",referencedColumnName = "etudiantPK")
    private List<Etudiant> etudiants;*/
}
