package com.test.univ.repository;

import com.test.univ.model.ObtientNote;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ObtientNoteRepository extends JpaRepository<ObtientNote,Long> {
}
