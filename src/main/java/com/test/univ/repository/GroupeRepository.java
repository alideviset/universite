package com.test.univ.repository;

import com.test.univ.model.Groupe;

import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupeRepository extends JpaRepository<Groupe,String> {
}
