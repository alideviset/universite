package com.test.univ.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Specialite implements Serializable {
    @Id
    private String codespec;
    private String codeanne;
    private String libfr;
    private String libar;
    private String niveau;
    @ManyToOne
    @JoinColumn(name = "ment_code")
    private Mention mention;
    @OneToMany(mappedBy = "specialite",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private Collection<UE> ues;
}
