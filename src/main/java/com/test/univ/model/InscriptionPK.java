package com.test.univ.model;

import lombok.*;


import javax.persistence.Embeddable;
import java.io.Serializable;


@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Embeddable
public class InscriptionPK implements Serializable {
    private String codeannee;
    private String Semestre ;
    private Integer CIN ;
    private String niveau;
    private String CodeSp ;
}
