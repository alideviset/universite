package com.test.univ.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Etablissement implements Serializable {
    @Id
    private String codeetab;
    private String libfr;
    private String libar;
    private String logo;
    @ManyToOne
    @JoinColumn(name = "org_code")
    //@JsonIgnore
    private Organisme organisme;
    @OneToMany(mappedBy = "etablissement",fetch = FetchType.LAZY,cascade = CascadeType.ALL)

    private Collection<Departement> departements;
   @JsonIgnore
    public Organisme getOrganisme() {
        return organisme;
    }
    @JsonSetter
    public void setOrganisme(Organisme organisme) {
        this.organisme = organisme;
    }
    @JsonIgnore
    public Collection<Departement> getDepartements() {
        return departements;
    }


}
