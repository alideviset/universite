package com.test.univ.controller;

import com.test.univ.model.Inscription;
import com.test.univ.repository.InscriptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/inscriptions")
public class InscriptionController {
    @Autowired
    private InscriptionRepository repository;

    @PostMapping
    public Inscription save(@RequestBody Inscription inscription){
        return repository.save(inscription);
    }

    @GetMapping
    public List<Inscription> getAll(){
        return repository.findAll();
    }
}
