package com.test.univ.controller;


import com.test.univ.model.Etablissement;
import com.test.univ.model.Gouvernorat;
import com.test.univ.model.Organisme;
import com.test.univ.repository.EtablissementRepository;
import com.test.univ.repository.OrganismeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

//import javax.validation.Valid;
import java.net.URI;
import java.util.*;

@RestController
@RequestMapping("/api/v1/organismes")
public class OrganismeController {
    @Autowired
    OrganismeRepository repository;
    @Autowired
    EtablissementRepository etablissementRepository;
    @Autowired
    public OrganismeController(OrganismeRepository repository,EtablissementRepository etablissementRepository){
        this.repository=repository;
        this.etablissementRepository=etablissementRepository;
    }
    @PostMapping
    public ResponseEntity<Organisme> create(@Validated @RequestBody Organisme organisme) {
        Organisme savedOrganisme = repository.save(organisme);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{codeorg}")
                .buildAndExpand(savedOrganisme.getCodeorg()).toUri();

        return ResponseEntity.created(location).body(savedOrganisme);
    }

    @PutMapping("/{codeorg}")
    public ResponseEntity<String> update(@PathVariable String codeorg, @Validated @RequestBody Organisme organisme) {
        Optional<Organisme> optionalOrganisme = repository.findById(codeorg);
        if (!optionalOrganisme.isPresent()) {
            return ResponseEntity.unprocessableEntity().build();
        }

        organisme.setCodeorg(optionalOrganisme.get().getCodeorg());

        repository.save(organisme);
        return new ResponseEntity<String>("Organisme with '"+codeorg+"' has been updated",HttpStatus.OK);
        //return ResponseEntity.noContent().build();
    }
    @DeleteMapping("/{codeorg}")
    public ResponseEntity<String> delete(@PathVariable String codeorg) {
        Optional<Organisme> optionalOrganisme = repository.findById(codeorg);
        if (!optionalOrganisme.isPresent()) {
            return ResponseEntity.unprocessableEntity().build();
        }

        repository.delete(optionalOrganisme.get());
        return new ResponseEntity<String>("Organisme with '"+codeorg+"' has been deleted",HttpStatus.OK);

    }

    @GetMapping("/{codeorg}")
    public ResponseEntity<Organisme> getById(@PathVariable String codeorg) {
        Optional<Organisme> org = repository.findById(codeorg);
        if (!org.isPresent()) {
            return ResponseEntity.unprocessableEntity().build();
        }

        return ResponseEntity.ok(org.get());
    }
    @GetMapping
    public ResponseEntity<Page<Organisme>> getAll(Pageable pageable) {
        return ResponseEntity.ok(repository.findAll(pageable));
    }
    @DeleteMapping
    public ResponseEntity<String> deleteAllOrganismes() {
        System.out.println("Delete All organisme...");
        repository.deleteAll();
        return new ResponseEntity<>("All organisme have been deleted!", HttpStatus.OK);
    }

    @GetMapping("/libfrs")
    public ResponseEntity<Map<String, Object>> getAllOrgs(
            @RequestParam(required = false) String libfr,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "3") int size
    ) {

        try {
            List<Organisme> organismes = new ArrayList<Organisme>();
            Pageable paging = PageRequest.of(page, size);

            Page<Organisme> pageTuts;
            if (libfr == null)
                pageTuts = repository.findAll(paging);
            else
                pageTuts = repository.findByLibfrContaining(libfr, paging);

            organismes = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("organismes", organismes);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

 /*   @DeleteMapping("delete/{codeorg}")
    public ResponseEntity<Organisme> deletee(@PathVariable String codeorg) {
        Optional<Organisme> optionalOrganisme = repository.findById(codeorg);
        if (!optionalOrganisme.isPresent()) {
            return ResponseEntity.unprocessableEntity().build();
        }

        deleteOrganismeInTransaction(optionalOrganisme.get());

        return ResponseEntity.noContent().build();
    }

    @Transactional
    public void deleteOrganismeInTransaction(Organisme organisme) {
        etablissementRepository.deleteByCodeorg(organisme.getCodeorg());
        repository.delete(organisme);
    }*/

}
