package com.test.univ.controller;

import com.test.univ.model.Departement;

import com.test.univ.model.Specialite;
import com.test.univ.repository.SpecialiteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class SpecialiteController {
    @Autowired
    SpecialiteRepository repository;

    public List<Specialite> getAllSpecialites() {
        System.out.println("Get all Specialite...");
        List<Specialite> Specialite = new ArrayList<>();
        repository.findAll().forEach(Specialite::add);
        return Specialite;
    }
}
