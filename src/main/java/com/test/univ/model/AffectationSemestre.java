package com.test.univ.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class AffectationSemestre implements Serializable {
    @Id
    private String codeannee;
    private String Semestre ;
    private Integer CIN ;
    private String NomGroupe;
    private Integer CodeSp ;

}
