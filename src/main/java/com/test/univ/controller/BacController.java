package com.test.univ.controller;

import com.test.univ.model.Bac;

import com.test.univ.repository.BacRepository;
import com.test.univ.service.BacServices;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

import java.util.*;


@RestController
@RequestMapping("/bacs")
public class BacController {
    @Autowired
    private BacRepository repository;
    @Autowired
    private BacServices service;

    @PostMapping("/savebac")
    public ResponseEntity<Bac> createBac(@RequestBody Bac bac) {
        try {
            Bac _bac = repository
                    .save(new Bac(bac.getCodebac(), bac.getLibbac() ));
            return new ResponseEntity<>(_bac, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping("/listbac")
    public ResponseEntity<List<Bac>> getAllBacs(@RequestParam(required = false) String libbac) {
        try {
            List<Bac> bc = new ArrayList<Bac>();

            if (libbac == null)
                repository.findAll().forEach(bc::add);
            else
                repository.findByLibbac(libbac).forEach(bc::add);

            if (bc.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(bc, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping("/bac/{codebac}")
    public ResponseEntity<Bac> getTutorialById(@PathVariable("codebac") String codebac) {
        Optional<Bac> bac = repository.findById(codebac);

        if (bac.isPresent()) {
            return new ResponseEntity<>(bac.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/updatebac/{codebac}")
    public ResponseEntity<Bac> updateBac( @PathVariable("codebac") String codebac,  @RequestBody Bac bac) {
        Optional<Bac> bc = repository.findById(codebac);
        if (bc.isPresent()) {
            Bac stdFromDb = bc.get();
            stdFromDb.setCodebac(bac.getCodebac());
            stdFromDb.setLibbac(bac.getLibbac());
            //String id = repository.save(stdFromDb);
            return new ResponseEntity<>(repository.save(stdFromDb), HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }
    @DeleteMapping("/deletebac/{codebac}")
    public ResponseEntity<String> deleteBac(@PathVariable("codebac") String codebac){
        repository.deleteById(codebac);
        return new ResponseEntity<String>("Bac with '"+codebac+"' has been deleted",HttpStatus.OK);
    }

    @DeleteMapping("/deleteallbac")
        public ResponseEntity<HttpStatus> deleteAllBacs() {
            try {
                repository.deleteAll();
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } catch (Exception e) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }



}
