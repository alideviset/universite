package com.test.univ.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class EC {
    @Id

    private String codeec;
    private String codeanne;
    private String uecode;
    private String libfr;
    private String abrev;
    private Float creditec;
    private Float coefec;
    private Integer regimecc;
    private String semestre;
    private Integer avecnp;
    private Integer aveccc;
    private Integer aveex;
    private Float pondnp;
    private Float pondcc;
    private Float pondex;
    private Float pondtp;
}
