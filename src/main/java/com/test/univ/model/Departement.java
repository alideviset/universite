package com.test.univ.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Departement implements Serializable {


    @Id
    private String codedept;
    private String libfr;
    private String libar;
    private String logo;
    @ManyToOne
    @JoinColumn(name = "etab_code")
    private Etablissement etablissement;
    @OneToMany(mappedBy = "departement",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private Collection<Domaine> domaines;

    public Etablissement getEtablissement() {
        return etablissement;
    }

    public Collection<Domaine> getDomaines() {
        return domaines;
    }
}
