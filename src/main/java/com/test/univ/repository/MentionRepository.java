package com.test.univ.repository;

import com.test.univ.model.Mention;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MentionRepository extends JpaRepository<Mention,String> {
}
