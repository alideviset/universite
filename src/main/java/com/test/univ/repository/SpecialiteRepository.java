package com.test.univ.repository;


import com.test.univ.model.Specialite;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SpecialiteRepository extends JpaRepository<Specialite,String> {
}
