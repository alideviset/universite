package com.test.univ.controller;

import com.test.univ.model.Departement;
import com.test.univ.model.EC;
import com.test.univ.repository.EcRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class EcController {
    @Autowired
    EcRepository repository;

    public List<EC> getAllEcs() {
        System.out.println("Get all EC...");
        List<EC> EC = new ArrayList<>();
        repository.findAll().forEach(EC::add);
        return EC;
    }
}
